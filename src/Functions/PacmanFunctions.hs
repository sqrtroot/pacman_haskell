module Functions.PacmanFunctions where
import           Prelude                 hiding ( lookup )
import           DTypes
import           Functions.MovementFunctions
import           Functions.TileFunctions
import           Data.Maybe
import           Numeric.Fixed
import           Functions.BoardFunctions
import           Graphics.Gloss.Interface.Pure.Game
                                         hiding ( Vector )
import           Data.HashMap.Lazy       hiding ( filter
                                                , foldr
                                                )

movePacman :: World -> Fixed -> World
movePacman w@World { pacman } f = do
  let p = fromMaybe pacman (moveInWorld w f pacman)
  followTeleporter (eatDots w { pacman = p })

followTeleporter :: World -> World
followTeleporter w@World { pacman, board }
  | teleportTimeout pacman > 0 = w
    { pacman = pacman { teleportTimeout = teleportTimeout pacman - 1 }
    }
  | otherwise = case flip getTile board . getPosition $ pacman of
    Just x -> case maybeHead (findMatchingTeleporters x board) of
      Just y -> w
        { pacman = pacman { pposition = tileCoordinate y, teleportTimeout = 10 }
        }
      Nothing -> w
    Nothing -> w

eatDots :: World -> World
eatDots w@World { pacman, board = b@Board { tiles } } = do
  let eatenDots = concatMap
        (filter (areColliding pacman) . filter (isDot . tileType))
        tiles
  let newBoard = foldr (\x acc -> updateTile x Empty acc) b eatenDots
  let addScore = foldr ((+) . dotToScore . tileType) 0 eatenDots
  w { board = newBoard, pacman = pacman { score = score pacman + addScore } }

directions :: HashMap Char Direction
directions = fromList [('w', North), ('a', West), ('s', South), ('d', East)]

pacmanEventHandler :: Event -> Pacman -> Pacman
pacmanEventHandler (EventKey (Char c) Down _ _) p@Pacman { pmovement } =
  case lookup c directions of
    Just x  -> p { pwantsDirection = Just pmovement { theta = x } }
    Nothing -> p

pacmanEventHandler (EventKey (Char c) Up _ _) p@Pacman { pmovement } =
  case lookup c directions of
    Just x ->
      if theta pmovement == x then p { pwantsDirection = Nothing } else p
    Nothing -> p

pacmanEventHandler _ p = p

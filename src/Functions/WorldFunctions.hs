module Functions.WorldFunctions where
import           DTypes
import           Data.List
import           Data.Dynamic
import           Functions.PacmanFunctions
import           Functions.GhostFunctions
import           Graphics.Gloss.Data.Picture
import           Functions.BoardFunctions
import           Functions.PacmanFunctions
import           Graphics.Gloss.Data.ViewPort
import           Graphics.Gloss.Interface.Pure.Game
import           Numeric.Fixed
import           Debug.Trace

drawWorld :: ViewPort -> World -> IO Picture
drawWorld viewPort World { board, pacman, ghosts } =
  return
    . applyViewPortToPicture viewPort
    . pictures
    $ [draw board, draw pacman, draw ghosts]

parseBoardFile :: String -> Maybe (Board, Coordinate, Coordinate)
parseBoardFile a = do
  let l = unlines . reverse . filter (not . isPrefixOf "--") . lines $ a
  board <- parseBoard l
  ps    <-
    tileCoordinate
      <$> (maybeHead . concatMap (filter ((PacmanSpawn ==) . tileType)) . tiles)
            board
  gs <-
    tileCoordinate
      <$> (maybeHead . concatMap (filter ((GhostSpawn ==) . tileType)) . tiles)
            board
  return (board, ps, gs)

loadWorld :: String -> Maybe World
loadWorld f = do
  (b, pstart, gstart) <- parseBoardFile f
  return World
    { board  = b
    , ghosts = [ Ghost { gposition       = gstart
                       , gmovement = PolarVector { theta = North, rho = 2 }
                       , ghosttype       = Pink
                       , gwantsDirection = Nothing
                       }
               , Ghost { gposition       = gstart
                       , gmovement = PolarVector { theta = North, rho = 2 }
                       , ghosttype       = Red
                       , gwantsDirection = Nothing
                       }
               , Ghost { gposition       = gstart
                       , gmovement = PolarVector { theta = North, rho = 2 }
                       , ghosttype       = Cyan
                       , gwantsDirection = Nothing
                       }
               , Ghost { gposition       = gstart
                       , gmovement = PolarVector { theta = North, rho = 2 }
                       , ghosttype       = Orange
                       , gwantsDirection = Nothing
                       }
               ]
    , pacman = Pacman { pposition       = pstart
                      , pmovement       = PolarVector { theta = North, rho = 5 }
                      , score           = 0
                      , lifes           = 3
                      , pwantsDirection = Nothing
                      , teleportTimeout = 0
                      }
    }

updateWorld :: Float -> World -> IO World
updateWorld f' w = do
  let f         = floatToFixed f'
  let newWorld  = movePacman w f
  let newWorld2 = moveGhosts newWorld f
  return newWorld2

handleEvents :: Event -> World -> IO World
handleEvents event w@World { pacman } = do
  let newP = pacmanEventHandler event pacman
  return w { pacman = newP }

module Functions.CoordinateFunctions where
import           DTypes
import           Debug.Trace
import           Numeric.Fixed


addVector :: Coordinate -> Vector -> Coordinate
addVector c z = RealCoordinate { rx = rx + xd, ry = ry + yd }
 where
  CartesianVector { x = xd, y = yd } = toCartesian z
  RealCoordinate { rx, ry }          = toRealCoordinate c

multiplyVector :: Vector -> Fixed -> Vector
multiplyVector CartesianVector { x, y } f =
  CartesianVector { x = x * f, y = y * f }
multiplyVector p@PolarVector { rho } f = p { rho = rho * f }

euclideanDistance :: Coordinate -> Coordinate -> Fixed
euclideanDistance RealCoordinate { rx = rxa, ry = rya } RealCoordinate { rx = rxb, ry = ryb }
  = sqrt (((rxb - rxa) ^ 2) + ((ryb - rya) ^ 2))
euclideanDistance a b =
  euclideanDistance (toRealCoordinate a) (toRealCoordinate b)

reverseDirection :: Direction -> Direction
reverseDirection North = South
reverseDirection South = North
reverseDirection East  = West
reverseDirection West  = East

distanceVector :: Coordinate -> Coordinate -> Vector
distanceVector (RealCoordinate x1 y1) (RealCoordinate x2 y2) =
  CartesianVector (abs x2 - x1) (abs y2 - y1)
distanceVector a b = distanceVector (toRealCoordinate a) (toRealCoordinate b)

rotateVector :: Vector -> Fixed -> Vector
rotateVector CartesianVector { x, y } a =
  CartesianVector { x = x * cos a - y * sin a, y = x * sin a + y * cos a }

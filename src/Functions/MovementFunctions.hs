module Functions.MovementFunctions where
import           DTypes
import           Functions.TileFunctions
import           Control.Monad
import           Data.Maybe
import           Functions.CoordinateFunctions
import           Debug.Trace
import           Numeric.Fixed
import           Data.Fixed              hiding ( Fixed )

moveInWorld
  :: (Show a, Moveable a, Placeable a, Collidable a)
  => World
  -> Fixed
  -> a
  -> Maybe a
moveInWorld World { board } f moveable = do
  let directionChange =
        wantsDirection moveable
          >>= ( checkIfOkMovement board
              . roundMovement
              . moveUnsafe f
              . flip setMovement moveable
              )
  case directionChange of
    Just x  -> return x
    Nothing -> checkIfOkMovement board . roundMovement . moveUnsafe f $ moveable

checkIfOkMovement :: (Collidable a) => Board -> a -> Maybe a
checkIfOkMovement board moved | isLegalPosition board moved = Just moved
                              | otherwise                   = Nothing

isLegalPosition :: (Collidable a) => Board -> a -> Bool
isLegalPosition (Board b) p = all
  null
  (map (filter (areColliding p) . filter (not . isAccessable . tileType)) b)

areColliding :: (Collidable a, Collidable b) => a -> b -> Bool
areColliding a b = ax1 < bx2 && ax2 > bx1 && ay1 > by2 && ay2 < by1
 where
  (RealCoordinate ax1 ay1, RealCoordinate ax2 ay2) = getBoundingBox a
  (RealCoordinate bx1 by1, RealCoordinate bx2 by2) = getBoundingBox b

moveUnsafe :: (Moveable a, Placeable a) => Fixed -> a -> a
moveUnsafe r = setPosition
  =<< liftM2 addVector getPosition (flip multiplyVector r . getMovement)

roundMovement :: (Moveable a, Placeable a) => a -> a
roundMovement a
  | y > 0 && (1 - ry `mod'` 1) < 0.1 = setPosition
    RealCoordinate { rx = rx, ry = fromInteger (ceiling ry) }
    a
  | y < 0 && (ry `mod'` 1) < 0.1 = setPosition
    RealCoordinate { rx = rx, ry = fromInteger (floor ry) }
    a
  | x > 0 && (1 - rx `mod'` 1) < 0.1 = setPosition
    RealCoordinate { rx =fromInteger (ceiling rx), ry = ry }
    a
  | x < 0 && (rx `mod'` 1) < 0.1 = setPosition
    RealCoordinate { rx =fromInteger (floor rx), ry = ry }
    a
  | otherwise = a
 where
  CartesianVector { x, y }  = getMovement a
  RealCoordinate { rx, ry } = getPosition a


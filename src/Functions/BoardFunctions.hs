module Functions.BoardFunctions where
import           DTypes
import           Functions.TileFunctions
import           Functions.CoordinateFunctions
import           Data.List

firstDot :: Board -> Maybe Coordinate
firstDot Board { tiles } = firstDot' tiles 0

firstDot' :: [[Tile]] -> Int -> Maybe Coordinate
firstDot' []       _ = Nothing
firstDot' (x : xs) y = case findIndex (isDot . tileType) x of
  Just t  -> Just (TileCoordinate t y)
  Nothing -> firstDot' xs (y + 1)


checkBoard :: Board -> Bool
checkBoard b = case firstDot b of
  Just fd -> not (hasDots (checkBoard' fd b))
  Nothing -> False

checkBoard' :: Coordinate -> Board -> Board
checkBoard' tc@TileCoordinate { tx = x, ty = y } b@Board { tiles }
  | not (isAccessable (tileType (tiles !! y !! x)))
  = b
  | otherwise
  = checkBoard' (TileCoordinate (max 0 $ x - 1) y)
    . checkBoard' (TileCoordinate (min (length (tiles !! y) - 1) $ x + 1) y)
    . checkBoard' (TileCoordinate x (max 0 $ y - 1))
    . checkBoard' (TileCoordinate x (min (length tiles - 1) $ y + 1))
    $ updateTileCoordinate tc Wall b
-- |Check if there are dots left on the board
hasDots :: Board -> Bool
hasDots (Board b) = (any . any) (isDot . tileType) b


parseBoard' :: Int -> Int -> String -> [Tile]
parseBoard' _ _ [] = []
parseBoard' x y (t : tx) =
  Tile (fromCharacter t) (TileCoordinate x y) : parseBoard' (x + 1) y tx

parseBoard'' :: Int -> Int -> [String] -> [[Tile]]
parseBoard'' _ _ []       = []
parseBoard'' x y (t : tx) = parseBoard' 0 y t : parseBoard'' 0 (y + 1) tx


parseBoard :: String -> Maybe Board
parseBoard s =
  let b = Board { tiles = parseBoard'' 0 0 (lines s) }
  in  if checkBoard b then Just b else Nothing

findMatchingTeleporters :: Tile -> Board -> [Tile]
findMatchingTeleporters t@Tile { tileType = Teleporter { teleporterName = c } }
  = filter (t /=)
    . filter ((c ==) . teleporterName . tileType)
    . filter
        ( (\case
            Teleporter _ -> True
            _            -> False
          )
        . tileType
        )
    . concat
    . tiles
findMatchingTeleporters _ = const []

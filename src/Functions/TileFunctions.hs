module Functions.TileFunctions where
import           DTypes
import Debug.Trace

getTile :: Coordinate -> Board -> Maybe Tile
getTile (TileCoordinate x y) Board { tiles }
  | y >= length tiles        = Nothing
  | x >= length (tiles !! y) = Nothing
  | otherwise                = Just (tiles !! y !! x)
getTile RealCoordinate{rx, ry} b = getTile(TileCoordinate (floor rx) (floor ry)) b

-- |Update type of tile
updateTile :: Tile -> TileType -> Board -> Board
updateTile tile@Tile { tileCoordinate = TileCoordinate { tx, ty } } ttype Board { tiles }
  = Board { tiles = ys ++ [xs ++ [tile { tileType = ttype }] ++ xe] ++ ye }
 where
  (ys, ym : ye) = splitAt ty tiles
  (xs, _ : xe ) = splitAt tx ym
updateTileCoordinate :: Coordinate -> TileType -> Board -> Board
updateTileCoordinate tc tt = updateTile (Tile{tileCoordinate=tc, tileType=tt}) tt

-- |Check if tile is dot type
isDot :: TileType -> Bool
isDot Bigdot   = True
isDot Smalldot = True
isDot _        = False

-- |Check if tile is accessable for player/ghost
isAccessable :: TileType -> Bool
isAccessable Wall          = False
isAccessable (Door Closed) = False
isAccessable HiddenWall    = False
isAccessable _             = True

fromCharacter :: Char -> TileType
fromCharacter '█' = Wall
fromCharacter '⊙' = Bigdot
fromCharacter '0' = Bigdot
fromCharacter '·' = Smalldot
fromCharacter '.' = Smalldot
fromCharacter 'p' = PacmanSpawn
fromCharacter 'g' = GhostSpawn
fromCharacter ' ' = Empty
fromCharacter '_' = HiddenWall
fromCharacter x   = Teleporter x

dotToScore :: TileType -> Int
dotToScore Bigdot = 5
dotToScore Smalldot = 2
dotToScore _ = 0

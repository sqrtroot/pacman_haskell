{-# LANGUAGE DuplicateRecordFields, DefaultSignatures, NamedFieldPuns, InstanceSigs #-}
module DTypes where
import           Graphics.Gloss.Data.Color
import           Graphics.Gloss.Data.Picture
                                         hiding ( Vector )
import           Control.Monad
import           Numeric.Fixed
import           GHC.Float
--   _____ _                         
--  / ____| |                        
-- | |    | | __ _ ___ ___  ___  ___ 
-- | |    | |/ _` / __/ __|/ _ \/ __|
-- | |____| | (_| \__ \__ \  __/\__ \
--  \_____|_|\__,_|___/___/\___||___/
--                                   

-- * Classes
class Collidable a where
  getBoundingBox :: a -> (Coordinate, Coordinate)
  default getBoundingBox :: (Placeable a) => a -> (Coordinate, Coordinate)
  getBoundingBox a = (RealCoordinate{rx=rx, ry=ry+1}, RealCoordinate{rx=rx+1,ry=ry})
    where RealCoordinate{rx, ry} = getPosition a


class Drawable a where
  draw :: a -> Picture
  default draw :: (ShowGraphic a, Placeable a) => a -> Picture
  draw a = translate (fixedToFloat rx) (fixedToFloat ry) (showGraphic a)
    where RealCoordinate {rx, ry} = getPosition a

class Moveable a where
  getMovement :: a -> Vector
  setMovement :: Vector -> a -> a
  wantsDirection :: a -> Maybe Vector

class ShowGraphic a where
  showGraphic :: a -> Picture

class Placeable a where
  getPosition :: a -> Coordinate
  setPosition :: Coordinate -> a -> a

--  _____        _     _______                    
-- |  __ \      | |   |__   __|                   
-- | |  | | __ _| |_ __ _| |_   _ _ __   ___  ___ 
-- | |  | |/ _` | __/ _` | | | | | '_ \ / _ \/ __|
-- | |__| | (_| | || (_| | | |_| | |_) |  __/\__ \
-- |_____/ \__,_|\__\__,_|_|\__, | .__/ \___||___/
--                           __/ | |              
--                          |___/|_|              
-- 

data Coordinate = RealCoordinate {rx::Fixed, ry::Fixed}
                | TileCoordinate {tx::Int, ty::Int}
  deriving(Eq,Show,Ord)

newtype Board = Board {tiles :: [[Tile]]}
  deriving(Show)

data Pacman = Pacman {pposition:: Coordinate, pmovement ::Vector, score :: Int, lifes :: Int, pwantsDirection :: Maybe Vector, teleportTimeout :: Int}
  deriving(Show)

data GhostType = Red
               | Pink
               | Cyan
               | Orange
  deriving(Enum, Eq, Show)

data Ghost = Ghost {gposition:: Coordinate, gmovement ::Vector, ghosttype :: GhostType, gwantsDirection :: Maybe Vector}
  deriving(Show)

data DoorState = Open
               | Closed
    deriving(Eq)

data TileType = Wall
        | Bigdot
        | Smalldot
        | Door {doorState::DoorState}
        | Teleporter {teleporterName :: Char}
        | GhostSpawn
        | PacmanSpawn
        | HiddenWall
        | Empty
    deriving(Eq)

data Tile = Tile {tileType :: TileType, tileCoordinate :: Coordinate}
    deriving(Eq, Show)

data Direction = North
               | East
               | South
               | West
  deriving(Eq, Show, Ord, Bounded, Enum)

data Vector = PolarVector {theta::Direction, rho::Fixed}
            | CartesianVector {x::Fixed, y::Fixed}
  deriving(Show)

data World = World{
    board :: Board,
    pacman :: Pacman,
    ghosts :: [Ghost]
}

--  _____           _                            
-- |_   _|         | |                           
--   | |  _ __  ___| |_ __ _ _ __   ___ ___  ___ 
--   | | | '_ \/ __| __/ _` | '_ \ / __/ _ \/ __|
--  _| |_| | | \__ \ || (_| | | | | (_|  __/\__ \
-- |_____|_| |_|___/\__\__,_|_| |_|\___\___||___/


instance Collidable Pacman
instance Drawable Pacman
instance Placeable Pacman where
  getPosition = toRealCoordinate . pposition
  setPosition a p = p { pposition = a }

instance Moveable Pacman where
  getMovement = toCartesian . pmovement
  setMovement a p = p { pmovement = a }
  wantsDirection = pwantsDirection

instance ShowGraphic Pacman where
  showGraphic Pacman { pmovement = PolarVector { theta, rho } } =
    rotate (90 * (fromIntegral . fromEnum $ theta)) $ pictures
      [ color yellow (circleSolid 0.5)
      , polygon [(0, 0), (-0.5, 0.5), (0.5, 0.5), (0, 0)]
      ]


instance Collidable Ghost
instance Placeable Ghost where
  getPosition = toRealCoordinate . gposition
  setPosition a p = p { gposition = a }

instance ShowGraphic Ghost where
  showGraphic Ghost { ghosttype = Red }    = baseGhost red
  showGraphic Ghost { ghosttype = Pink }   = baseGhost (makeColor 1 0 1 1)
  showGraphic Ghost { ghosttype = Cyan }   = baseGhost cyan
  showGraphic Ghost { ghosttype = Orange } = baseGhost orange

instance Drawable Ghost
instance Drawable [Ghost] where
  draw a = pictures $ map draw a

instance Moveable Ghost where
  getMovement = toCartesian . gmovement
  setMovement a g = g { gmovement = a }
  wantsDirection = gwantsDirection


-- instance Show Board where
--     show (Board b) = unlines (map (concatMap (show . tileType)) b)

instance Drawable Board where
  draw (Board b) = pictures $ (concatMap . map) draw b


instance Show TileType where
  show Wall           = "█"
  show Bigdot         = "⊙"
  show Smalldot       = "·"
  show Empty          = " "
  show (Teleporter _) = " "
  show _              = ""

instance ShowGraphic TileType where
  showGraphic Wall     = color blue (polygon $ rectanglePath 1 1)
  showGraphic Bigdot   = color yellow (circleSolid 0.45)
  showGraphic Smalldot = color (dark yellow) (circleSolid 0.25)
  showGraphic _        = Blank


instance ShowGraphic Tile where
  showGraphic = showGraphic . tileType

instance Placeable Tile where
  getPosition Tile { tileCoordinate } = toRealCoordinate tileCoordinate
  setPosition tc@(TileCoordinate _ _) t = t { tileCoordinate = tc }
  setPosition (   RealCoordinate _ _) t = undefined

instance Collidable Tile
instance Drawable Tile

--  ______                _   _                 
-- |  ____|              | | (_)                
-- | |__ _   _ _ __   ___| |_ _  ___  _ __  ___ 
-- |  __| | | | '_ \ / __| __| |/ _ \| '_ \/ __|
-- | |  | |_| | | | | (__| |_| | (_) | | | \__ \
-- |_|   \__,_|_| |_|\___|\__|_|\___/|_| |_|___/
--                                              


toRealCoordinate :: Coordinate -> Coordinate
toRealCoordinate TileCoordinate { tx, ty } =
  RealCoordinate (fromIntegral tx) (fromIntegral ty)
toRealCoordinate a@(RealCoordinate _ _) = a

toCartesian
  :: Vector -- ^ 
  -> Vector -- ^
toCartesian PolarVector { theta = North, rho } =
  CartesianVector { x = 0, y = 1 * rho }
toCartesian PolarVector { theta = East, rho } =
  CartesianVector { x = 1 * rho, y = 0 }
toCartesian PolarVector { theta = South, rho } =
  CartesianVector { x = 0, y = -1 * rho }
toCartesian PolarVector { theta = West, rho } =
  CartesianVector { x = -1 * rho, y = 0 }
toCartesian a@(CartesianVector _ _) = a

fixedToFloat :: Fixed -> Float
fixedToFloat = double2Float . fromFixed
floatToFixed :: Float -> Fixed
floatToFixed = toFixed . float2Double

baseGhost :: Color -> Picture
baseGhost clr = scale 0.1 0.1 $ pictures
  [ color clr $ circleSolid 3
  , color clr $ polygon
    [ (3 , 0)
    , (3 , -5)
    , (2 , -4)
    , (1 , -5)
    , (0 , -4)
    , (-1, -5)
    , (-2, -4)
    , (-3, -5)
    , (-3, 0)
    ]
  , translate (-1.5) 1 $ color white $ circleSolid 0.8
  , translate 1.5 1 $ color white $ circleSolid 0.8
  ]

maybeHead :: [a] -> Maybe a
maybeHead []      = Nothing
maybeHead (a : _) = Just a

module Constants where

defaultWorldPath :: String
defaultWorldPath = "resources/DefaultBoard.board"
pacmanSpeed :: Int
pacmanSpeed = 5
ghostSpeed :: Int
ghostSpeed = 4
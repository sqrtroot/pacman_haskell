module Functions.GhostFunctions where
import           DTypes
import           Numeric.Fixed
import           Data.List
import           Data.Maybe
import           Debug.Trace
import           Functions.CoordinateFunctions
import           Functions.MovementFunctions

-- https://youtu.be/ataGotQ7ir8?t=431
additionTable :: Direction -> Vector
additionTable North = CartesianVector (-1) 1
additionTable East  = CartesianVector 1 0
additionTable South = CartesianVector 0 (-1)
additionTable West  = CartesianVector (-1) 0

targetPosition :: World -> Ghost -> Coordinate
targetPosition World { pacman } Ghost { ghosttype = Red } = getPosition pacman

targetPosition World { pacman = pacman@Pacman { pmovement } } Ghost { ghosttype = Pink }
  = getPosition pacman
    `addVector` (additionTable (theta pmovement) `multiplyVector` 4)

targetPosition World { pacman } Ghost { gposition, ghosttype = Orange }
  | euclideanDistance (getPosition pacman) gposition > 8 = getPosition pacman
  | otherwise = TileCoordinate 0 0

targetPosition w@World { pacman = pacman@Pacman { pmovement } } Ghost { ghosttype = Cyan }
  = let intermediate =
            getPosition pacman
              `addVector` (additionTable (theta pmovement) `multiplyVector` 2)
        Ghost { gposition = bpos } = findBlinky w
    in  addVector intermediate
                  ((distanceVector intermediate bpos) `rotateVector` 180)

findBlinky :: World -> Ghost
findBlinky = head . filter ((Red ==) . ghosttype) . ghosts

nextMovement :: World -> Ghost -> Ghost
nextMovement w@World { board } g@Ghost { gmovement = PolarVector { theta, rho }, gposition }
  = do
    let options =
          [ PolarVector { theta = x, rho = 4 }
          | x <- [North .. West]
          , x /= reverseDirection theta
          , isLegalPosition
            board
            g
              { gposition = gposition
                              `addVector` PolarVector { theta = x, rho = 1 }
              }
          ]
    let bestOption = sortOn
          (euclideanDistance (targetPosition w g) . addVector gposition)
          options
    g { gwantsDirection = maybeHead bestOption }

moveGhosts :: World -> Fixed -> World
moveGhosts w@World { ghosts } f = w
  { ghosts =
    map
      (\g -> fromMaybe
        (g
          { gmovement =
            (gmovement g) { theta = reverseDirection . theta . gmovement $ g }
          }
        )
        (moveInWorld w f . nextMovement w $ g)
      )
      ghosts
  }

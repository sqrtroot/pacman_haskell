module Main where
import           DTypes
import           Functions.WorldFunctions
import           Constants
import           Control.Monad
import           Data.Maybe
import           System.IO
import           Graphics.Gloss.Interface.IO.Game
import           Graphics.Gloss.Data.Color
import           Graphics.Gloss.Data.ViewPort
import           Debug.Trace

viewPort = viewPortInit { viewPortScale = 12 }

main :: IO ()
main = do
  x <- loadWorld <$> readFile defaultWorldPath
  case x of
    Just x ->
      playIO (InWindow "P'CM'N" (512, 512) (1, 1)) black 60 x (drawWorld viewPort) handleEvents updateWorld
    Nothing -> error "Couldn't load board"
  return ()
